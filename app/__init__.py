from app.config import Config
import flask_uploads

from flask import Flask

images = flask_uploads.UploadSet('images', flask_uploads.IMAGES)

app = Flask(__name__, static_url_path='')
app.config.from_object(Config)

flask_uploads.configure_uploads(app, images)
# 8 MiB file upload limit
flask_uploads.patch_request_class(app, 8 * 1024 * 1024)


from app import routes

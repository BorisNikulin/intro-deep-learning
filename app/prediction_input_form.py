from app import images
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired


class PredictionInput(FlaskForm):
    image = FileField(validators=[
        FileRequired(),
        FileAllowed(images, 'Images only!')
    ])

import os

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'default-52769965'

    UPLOADS_DEFAULT_DEST= 'app/uploads'

import app.pytorch_agent_test as dqn

from typing import Text
import os

from skimage import transform
from skimage.color import rgb2gray
from PIL import Image
import numpy as np
import torch
import torch.optim as optim

def get_game_state(image):
    # preprocess the image before input into neural network
    # make image grayscale
    state = rgb2gray(image)
    # Normalizing, usually not a good idea to use amax since state might not contain max value
    # a better normalization formula is x - min(x) - average(x) / max(x) - min(x) since it centers around 0
    state = state.astype("float32") / np.amax(state)
    state = transform.resize(state, [160, 160])
    state = np.expand_dims(state, axis=0)
    return state

def test(state, model):
    optimizer = optim.Adam(model.parameters(), lr=.0001)
    temperature = 1.0
    checkpoint = torch.load("app/model1", map_location='cpu')
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    epoch = checkpoint['epoch']
    loss = checkpoint['loss']

    print(epoch)
    print(loss)
    return model.softmax_action(state, temperature)

def agent_predict(filename) -> Text:
    input_shape = (1, 160, 160)
    n_outputs = 3
    learning_rate = 0.0001
    gamma = 0.95
    temperature = 12.0
    epsilon = 0.99
    epsilon_decay = 0.001
    device = torch.device('cpu')

    model = dqn.DeepQNetwork(input_shape, n_outputs, learning_rate, gamma, temperature, epsilon, epsilon_decay).to(
        device)

    image = Image.open(f'app/uploads/images/{filename}').resize((256, 240))
    state = get_game_state(np.asarray(image, dtype='int32'))
    state = model.convert_to_tensor(state)
    action, _ = test(state, model)
    return str(action)
    # return "result"

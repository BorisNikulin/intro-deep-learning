from app import app

from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired

class NameForm(FlaskForm):
    """Form that asks for a name."""
    name = StringField('Name', [
            DataRequired()
        ])


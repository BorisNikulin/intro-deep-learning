from app import app, images
from app.prediction_input_form import PredictionInput
from app.predict import agent_predict

import os

from flask import render_template, send_from_directory, send_file


@app.route('/', methods=['GET'])
def frontpage():
    """Frontpage endpoint with [roject description
    and model prediction input form.
    """

    return render_template('frontpage.html', input_form=PredictionInput())

@app.route('/predict', methods=['GET', 'POST'])
def predict():
    input_form = PredictionInput()
    if input_form.validate_on_submit():
        image = input_form.image.data
        # filename = secure_filename(image.filename)
        filename = images.save(image)

        # TODO: do prediction/inference on the image

        result = agent_predict(filename)

        # flask uploads doesnt want to serve the file even though it did setup a blueprint to do so...
        # return render_template('result.html', image_url=images.url(filename))
        return render_template('result.html', image_filename=filename, result=result)

    return render_template('frontpage.html', input_form=input_form)

@app.route('/image/<path:filename>', methods=['GET', 'POST'])
def prediction_input_image(filename):
    # seems like send_from_dir/send_file etc are using this module folder as the base dir
    # ie the app folder is included instead of from the git base dir/over all project base dir
    # Made the default file upload dir include app so send_from_dir would work except not
    # for how flask-uploads uses it for some reason so here is the hardcoded version of it


    # more or less flask-uploads code for serving files
    config = app.upload_set_config.get('images')
    # print(config.destination)
    # root_dir = os.path.dirname(os.getcwd())
    # print(os.path.join(root_dir, 'upload', 'images'))
    # return send_from_directory(os.path.join(root_dir, 'upload', 'images'), filename)
    # print(filename)
    # return send_file('uploads/images/' + filename)
    return send_from_directory('uploads/images', filename)
    # return send_from_directory(config.destination, filename)

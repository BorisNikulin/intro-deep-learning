"""Exposes WSGI application object named :data:`app <server.app>`
for use by a server.

.. autodata:: app
"""
from app import app

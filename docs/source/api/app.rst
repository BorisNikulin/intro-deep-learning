app package
===========

Submodules
----------

app.config module
-----------------

.. automodule:: app.config
   :members:
   :undoc-members:
   :show-inheritance:

app.name\_form module
---------------------

.. automodule:: app.name_form
   :members:
   :undoc-members:
   :show-inheritance:

app.predict module
------------------

.. automodule:: app.predict
   :members:
   :undoc-members:
   :show-inheritance:

app.prediction\_input\_form module
----------------------------------

.. automodule:: app.prediction_input_form
   :members:
   :undoc-members:
   :show-inheritance:

app.pytorch\_agent\_test module
-------------------------------

.. automodule:: app.pytorch_agent_test
   :members:
   :undoc-members:
   :show-inheritance:

app.routes module
-----------------

.. automodule:: app.routes
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: app
   :members:
   :undoc-members:
   :show-inheritance:
